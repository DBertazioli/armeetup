## Old query:

questa sezione riporta a puro titolo informativo alcune parti del progetto in un primo momento svolto con Neo4j (quindi Cypher scripts, query in Cypher e file di config customizzati).

#### Per maggiori informazioni sull'implementazione con Neo4j è possibile visitare questa [repo](https://github.com/DBertazioli/NeoMeetup)
