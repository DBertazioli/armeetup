<div align = "center">
<p> An English version of the Readme and Report will be available soon. As a temporary workaround, please refer to the <a href= "https://gitlab.com/DBertazioli/armeetup/blob/master/presentation/prese_meetup.pdf">slides</a>.
</p> 
<p align="center">
  <a href="http://datascience.disco.unimib.it/it/"><img src = "https://raw.githubusercontent.com/malborroni/Foundations_of_Computer-Science/master/images/DSunimib.png" width = "100%"></a>
</p>

<p align="center">
  <a><img src = "https://raw.githubusercontent.com/DBertazioli/ARmeetup/master/report/images/logo_readme.png" width = "50%""></a>
</p>
<h4 align = "center">Analyze Meetup Social Network in ArangoDB</h4> 
    <h6 align = "center">A Data Management Project</h6> 


<p align="center">
  <a href="#overview">Overview &nbsp;</a> |
  <a href="#visualizations">&nbsp; Visualizations &nbsp;</a> |
  <a href="#report">&nbsp; Report &nbsp;</a> |
  <a href="#aboutus">&nbsp; About us &nbsp;</a>
</p>
</div>

<a name="overview"></a>
## &#9741; &nbsp; Overview
Meetup è una piattaforma che permette ai propri utenti di partecipare ad eventi organizzati da gruppi locali, con argomento i topic più disparati come ad esempio tecnologia, eventi sociali, spettacoli, formazione professionale e tanto altro.<br>
Il nostro progetto è consistito nell'acquisire le sottoscrizioni degli utenti ai vari eventi presenti sulla piattaforma (in forma streaming), a livello globale.
<br>

<a name="visualizations"></a>
## &#9741; &nbsp; Visualizations
Per riassumere gli aspetti più interessanti incontrati durante la nostra esplorazione dei dati abbiamo realizzato alcune visualizzazioni:

<ol>
  <li> Una visualizzazione statica, sotto forma di <a href = "https://raw.githubusercontent.com/DBertazioli/ARmeetup/master/report/images/heatmap_with_barplot_v3.jpg">heatmap</a> </li>
  <li> Una visualizzazione dinamica, realizzata sotto forma di <a href = "https://dbertazioli.github.io/Interact/">mappa interattiva </a>,divisa in due parti </li>
</ol>

<a name="report"></a>
## &#9741; &nbsp; Report
Come conclusione del nostro progetto e della nostra esplorazione abbiamo redatto un piccolo [report](https://gitlab.com/DBertazioli/armeetup/raw/master/report/report.pdf?inline=false) per riassumere misure rilevate e metodologie adottate.

<a name="aboutus"></a>
## &#9741; &nbsp; About us

&#8860; &nbsp; **Dario Bertazioli**

- *Cosa studio*: Studente Magistrale di Data Science presso l'Università degli Studi di Milano-Bicocca;
- *Studi precedenti*: Laurea triennale in Fisica presso l'Università degli Studi di Milano.
<br>

<div align = "center">
<p align = "center">
  <a href = "https://www.linkedin.com/in/dario-bertazioli-961ab4180/"><img src="https://raw.githubusercontent.com/DBertazioli/Interact/master/img/iconfinder_Popular_Social_Media-22_2329259.png" width = "3%"></a>
  <a href = "https://github.com/DBertazioli/"><img src="https://raw.githubusercontent.com/malborroni/Foundations_of_Computer-Science/master/images/GitHub.png" width = "3%"></a>
</div>

&#8860; &nbsp; **Fabrizio D'Intinosante**

- *Cosa studio*: Studente Magistrale di Data Science presso l'Università degli Studi di Milano-Bicocca;
- *Studi precedenti*: Laurea triennale in Economia e Statistica per le organizzazioni presso l'Università degli Studi di Torino.
<br>

<div align = "center">
<p align = "center">
  <a href = "https://www.linkedin.com/in/fabrizio-d-intinosante-125042180/"><img src="https://raw.githubusercontent.com/DBertazioli/Interact/master/img/iconfinder_Popular_Social_Media-22_2329259.png" width = "3%"></a>
  <a href = "https://github.com/faber6911/"><img src="https://raw.githubusercontent.com/malborroni/Foundations_of_Computer-Science/master/images/GitHub.png" width = "3%"></a>
</div>

&#8860; &nbsp; **Massimiliano Perletti**

- *Cosa studio*: Studente Magistrale di Data Science presso l'Università degli Studi di Milano-Bicocca;
- *Studi precedenti*: Laurea triennale in Ingegneria dei materiali e delle nano-tecnologie presso il Politecnico di Milano.
<br>

<div align = "center">
<p align = "center">
  <a href = "https://www.linkedin.com/in/massimilianoperletti/"><img src="https://raw.githubusercontent.com/DBertazioli/Interact/master/img/iconfinder_Popular_Social_Media-22_2329259.png" width = "3%"></a>
  <a href = "https://github.com/maxi93/"><img src="https://raw.githubusercontent.com/malborroni/Foundations_of_Computer-Science/master/images/GitHub.png" width = "3%"></a>
 </div> 

<div align = "right">
<p align = "right">
  <a href = "https://github.com/malborroni/">
             <img src = "https://raw.githubusercontent.com/DBertazioli/ARmeetup/master/report/images/credits_readme.png" width = "25%">
             </a>
</p>
</div>